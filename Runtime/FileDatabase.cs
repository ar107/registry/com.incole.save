﻿using Newtonsoft.Json;
using System.IO;
using UnityEngine;

public class FileDatabase
{
    private string _path;

    public FileDatabase(string path)
    {
        _path = path;
    }

    public JsonFileDatabaseCollection<T> GetJsonStoredCollection<T>() where T : class
    {
        return GetJsonStoredCollection<T>(typeof(T).Name);
    }

    public JsonFileDatabaseCollection<T> GetJsonStoredCollection<T>(string collection) where T : class
    {
        return new JsonFileDatabaseCollection<T>(Path.Combine(_path, collection));
    }

    public void Save<T>(string filename, T value, string pathPart = null) where T : class
    {
        var file = _path;

        if (pathPart != null)
            file = Path.Combine(file, pathPart);

        if (!Directory.Exists(file))
            Directory.CreateDirectory(file);

        file = Path.Combine(file, filename);

        if (File.Exists(file))
            File.Delete(file);

        var data = JsonConvert.SerializeObject(value, JsonSaveSerializeDefaults.SerializerSettings);

        File.WriteAllText(file, data);

        Debugr.Log(this, $"Data saved to file {file}");
    }

    public void Save(string filename, byte[] data, string pathPart = null)
    {
        var file = _path;

        if (pathPart != null)
            file = Path.Combine(file, pathPart);

        file = Path.Combine(file, filename);

        Directory.CreateDirectory(Path.GetDirectoryName(file));

        if (File.Exists(file))
            File.Delete(file);

        File.WriteAllBytes(file, data);

        Debugr.Log(this, $"Data saved to file {file}");
    }

    public T Load<T>(string filename) where T: class
    {
        if (!Directory.Exists(_path))
            return null;

        var file = Path.Combine(_path, filename);

        if (!File.Exists(file))
            return null;

        var data = File.ReadAllText(file);

        Debugr.Log(this, $"Data loaded from file {file}");

        return JsonConvert.DeserializeObject<T>(data, JsonSaveSerializeDefaults.SerializerSettings);
    }

    public byte[] Load(string filename)
    {
        if (!Directory.Exists(_path))
            return null;

        var file = Path.Combine(_path, filename);

        if (!File.Exists(file))
            return null;

        return File.ReadAllBytes(file);
    }

    public bool Exists(string filename)
    {
        if (!Directory.Exists(_path))
            return false;

        var file = Path.Combine(_path, filename);

        return File.Exists(file);
    }

    public void Remove(string filename)
    {
        var file = Path.Combine(_path, filename);

        File.Delete(file);
    }
}
