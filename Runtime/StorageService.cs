﻿using System;
using UnityEngine;

public class StorageService
{
    private FileDatabase _db;

    public StorageService(FileDatabase fileDatabase)
    {
        _db = fileDatabase;
    }

    public bool Exists(string filename)
    {
        return _db.Exists(filename);
    }

    public void Save(byte[] data, string filename)
    {
        _db.Save(filename, data);
    }

    public void Save<T>(T data, string filename) where T : class
    {
        _db.Save(filename, data);
    }

    public void Remove(string filename)
    {
        _db.Remove(filename);
    }

    public void Save<T>(T data, Guid id) where T : class
    {
        _db.Save(id.ToString(), data, data.GetType().Name);
    }

    public T Load<T>(Guid id) where T: class
    {
        var key = string.Format($"{typeof(T).Name}/{id}");
        
        return _db.Load<T>(key);
    }

    public T Load<T>(string filename) where T : class
    {
        return _db.Load<T>(filename);
    }

    public byte[] Load(string filename)
    {
        return _db.Load(filename);
    }
}