﻿using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

public class JsonFileDatabaseCollection<T> where T : class
{
    private string _path;

    internal JsonFileDatabaseCollection(string path)
    {
        _path = path;
    }

    public T Get(long id)
    {
        return Get(id.ToString());
    }

    public T Get(int id)
    {
        return Get(id.ToString());
    }

    public T Get(Guid id)
    {
        return Get(id.ToString());
    }
    
    public async UniTask<T[]> All()
    {
        if (!Directory.Exists(_path))
            return new T[0];

        var files = Directory.GetFiles(_path);

        if (files.Length == 0)
            return null;

        var data = new List<T>();

        foreach (var file in files)
        {
            data.Add(Get(file));
            await UniTask.NextFrame();
        }

        return data.ToArray();
    }

    public void Save(Guid id, T value)
    {
        Save(id.ToString(), value);
    }

    public void Save(long id, T value)
    {
        Save(id.ToString(), value);
    }

    public void Save(int id, T value)
    {
        Save(id.ToString(), value);
    }

    public void Save(string id, T value)
    {
        if (!Directory.Exists(_path))
            Directory.CreateDirectory(_path);

        var file = Path.Combine(_path, id);

        if (File.Exists(file))
            File.Delete(file);

        var data = JsonConvert.SerializeObject(value, JsonSaveSerializeDefaults.SerializerSettings);

        File.WriteAllText(file, data);
    }

    private T Get(string id)
    {
        if (!Directory.Exists(_path))
            return null;

        var file = Path.Combine(_path, id);

        if (!File.Exists(file)) 
            return null;

        var data = File.ReadAllText(file);

        return JsonConvert.DeserializeObject<T>(data, JsonSaveSerializeDefaults.SerializerSettings);
    }

    public T First()
    {
        if (!Directory.Exists(_path))
            return null;

        var ids = Directory.GetFiles(_path);

        if (ids.Length == 0)
            return null;

        return Get(ids[0]);
    }
}